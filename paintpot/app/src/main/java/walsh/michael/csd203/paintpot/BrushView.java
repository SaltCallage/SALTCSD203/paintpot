package walsh.michael.csd203.paintpot;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class BrushView extends View {
    //Properties for the paint tool
    public int brushColor = Color.BLUE;
    public float strokeWidth = 15f;
    private int currentLayer = 0;

    private DrawAction LastAction;
    private int LastActionLayer;

    private final int CLICK_TIME = 500;
    private long lastTouch;

    public Paint brush = new Paint();
    private Path path = new Path();

    private ArrayList<DrawAction>[] Layers = new ArrayList[3];


    class DrawAction
    {
        public DrawAction lastAction;
        public int lastActionLayer;
        public int getType(){
            return 0;
        }
    }

    class CirclePoint extends DrawAction{
        CirclePoint(float xp,float yp,float rad,Paint painter)
        {
            x = xp;
            y = yp;
            r = rad;
            paint = painter;
        }
        public float x;
        public float y;
        public float r;
        public Paint paint;

        @Override
        public int getType()
        {
            return 1;
        }
    }

    class PathPoint extends DrawAction{
        PathPoint(Paint b, Path p)
        {
            brush = b;
            path = p;
        }
        public Paint brush;
        public Path path;

        @Override
        public int getType()
        {
            return 2;
        }
    }



    public BrushView(Context context) {
        super(context);
        CalibrateBrush();
        Layers[0] = new ArrayList<DrawAction>();
        Layers[1] = new ArrayList<DrawAction>();
        Layers[2] = new ArrayList<DrawAction>();
    }


    public void UpdateBrushColor(int newColor)
    {
        brushColor = newColor;
        CalibrateBrush();
    }

    public void UpdateStrokeWidth(int size)
    {
        strokeWidth = size;
        CalibrateBrush();
    }

    public void UpdateLayer(int newLayer)
    {
        if(newLayer > 2)
            currentLayer = 2;
        else if(newLayer < 0)
            currentLayer = 0;
        else
            currentLayer = newLayer;
    }

    public int GetCurrentLayer()
    {
        return currentLayer;
    }

    public void EraseLayer(int layerToErase)
    {
        System.out.println("Seee taht shit?");
        Layers[layerToErase].clear();
        invalidate();
    }

    public void Undo()
    {
        if(LastAction == null)
            return;
        DrawAction newLastAction = LastAction.lastAction;
        int newLastActionLayer = LastAction.lastActionLayer;

        Layers[LastActionLayer].remove(LastAction);
        LastAction = newLastAction;
        LastActionLayer = newLastActionLayer;
        invalidate();
    }

    private void CalibrateBrush()
    {
        brush.setAntiAlias(true);
        brush.setColor(brushColor);
        brush.setStyle(Paint.Style.STROKE);
        brush.setStrokeJoin(Paint.Join.ROUND);
        brush.setStrokeWidth(strokeWidth);
    }

        @Override
    public boolean onTouchEvent(MotionEvent event) {
        float pointX = event.getX();
        float pointY = event.getY();

        // Checks for the event that occurs
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                path.moveTo(pointX, pointY);
                lastTouch = System.currentTimeMillis();
                return true;
            case MotionEvent.ACTION_MOVE:
                path.lineTo(pointX, pointY);
                break;
            case MotionEvent.ACTION_UP:
                PathMeasure stick = new PathMeasure(path, false);
                System.out.println(stick.getLength());
                if(System.currentTimeMillis() < lastTouch + CLICK_TIME && stick.getLength() < 10 ) {
                    CirclePoint newCircle = new CirclePoint(pointX, pointY, strokeWidth, brush);
                    if(LastAction != null)
                    {
                        newCircle.lastAction = LastAction;
                        newCircle.lastActionLayer = LastActionLayer;
                    }
                    LastAction = newCircle;
                    LastActionLayer = currentLayer;
                    Layers[currentLayer].add(newCircle);
                    brush = new Paint();
                    path = new Path();
                    CalibrateBrush();

                }
                else
                {
                    PathPoint newPath = new PathPoint(brush, path);
                    if(LastAction != null) {
                        newPath.lastAction = LastAction;
                        newPath.lastActionLayer = LastActionLayer;
                    }
                    LastAction = newPath;
                    LastActionLayer = currentLayer;
                    Layers[currentLayer].add(newPath);
                    brush = new Paint();
                    path = new Path();
                    CalibrateBrush();
                }

                break;
            default:
                return false;
        }
        // Force a view to draw.
        postInvalidate();
        return false;

    }
    @Override
    protected void onDraw(Canvas canvas) {
        for(int j = 0; j < 3; j++)
        {
            for (int i = 0; i < Layers[j].size(); i++)
            {
                switch (Layers[j].get(i).getType())
                {
                    case 2:
                        canvas.drawPath(((PathPoint) Layers[j].get(i)).path, (((PathPoint) Layers[j].get(i)).brush));
                        break;
                    case 1:
                        canvas.drawCircle(((CirclePoint) Layers[j].get(i)).x, ((CirclePoint) Layers[j].get(i)).y, ((CirclePoint) Layers[j].get(i)).r, ((CirclePoint) Layers[j].get(i)).paint);
                        break;
                }
            }
        }

        canvas.drawPath(path, brush);
    }

}
