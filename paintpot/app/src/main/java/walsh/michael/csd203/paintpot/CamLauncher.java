package walsh.michael.csd203.paintpot;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class CamLauncher extends AppCompatActivity {
    public View v;
    int CAM_CODE = 1337;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ImageButton btnCam;

        final Button btnBlue;
        final Button btnGreen;
        final Button btnRed;

        final ImageButton btnBig;
        final ImageButton btnHuge;
        final ImageButton btnSmall;

        final ImageButton btnLay0;
        final ImageButton btnLay1;
        final ImageButton btnLay2;

        ImageButton newFileButton;

        ImageButton undoButton;

        final TextView layerIndicator;



        final BrushView view=new BrushView(this);
        setContentView(view);
        v = view;
        View totes = View.inflate(this, R.layout.buttons,  null);
        addContentView(totes, new ViewGroup.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));

        v.setBackground(ContextCompat.getDrawable(this, R.drawable.cat));


        btnCam = findViewById(R.id.camButton);
        btnCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();

            }
        });

        btnRed = findViewById(R.id.redBtn);
        btnBlue = findViewById(R.id.blueBtn);
        btnGreen = findViewById(R.id.greenBtn);

        newFileButton = findViewById(R.id.NewFile);

        undoButton = findViewById(R.id.undo);

        btnLay0 = findViewById(R.id.layer0);
        btnLay1 = findViewById(R.id.layer1);
        btnLay2 = findViewById(R.id.layer2);

        btnLay1.setColorFilter(Color.argb(127, 127, 127, 127));
        btnLay2.setColorFilter(Color.argb(127, 127, 127, 127));
        btnLay0.setColorFilter(Color.argb(155, 255, 127, 127));


        btnRed.setText("");
        btnBlue.setText("BLUE");
        btnGreen.setText("");

        btnBig = findViewById(R.id.bigBtn);
        btnHuge = findViewById(R.id.hugeBtn);
        btnSmall = findViewById(R.id.smallBtn);


        view.UpdateStrokeWidth(20);
        btnBig.setColorFilter(Color.argb(155, 255, 127, 127));
        btnHuge.setColorFilter(Color.argb(155, 127, 127, 127));
        btnSmall.setColorFilter(Color.argb(155, 127, 127, 127));

        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.Undo();

            }
        });

        newFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.EraseLayer(0);
                view.EraseLayer(1);
                view.EraseLayer(2);

            }
        });

        btnRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.UpdateBrushColor(Color.RED);
                btnRed.setText("RED");
                btnBlue.setText("");
                btnGreen.setText("");

            }
        });


        btnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.UpdateBrushColor(Color.BLUE);
                btnRed.setText("");
                btnBlue.setText("BLUE");
                btnGreen.setText("");
            }
        });

        btnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.UpdateBrushColor(Color.GREEN);
                btnRed.setText("");
                btnBlue.setText("");
                btnGreen.setText("GREEN");
            }
        });


        btnBig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.UpdateStrokeWidth(20);
                btnBig.setColorFilter(Color.argb(155, 255, 127, 127));
                btnHuge.setColorFilter(Color.argb(127, 127, 127, 127));
                btnSmall.setColorFilter(Color.argb(127, 127, 127, 127));
            }
        });
        btnHuge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.UpdateStrokeWidth(30);
                btnBig.setColorFilter(Color.argb(127, 127, 127, 127));
                btnHuge.setColorFilter(Color.argb(155, 255, 127, 127));
                btnSmall.setColorFilter(Color.argb(127, 127, 127, 127));
            }
        });
        btnSmall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.UpdateStrokeWidth(5);
                btnBig.setColorFilter(Color.argb(127, 127, 127, 127));
                btnHuge.setColorFilter(Color.argb(127, 127, 127, 127));
                btnSmall.setColorFilter(Color.argb(155, 255, 127, 127));
            }
        });
        btnLay0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.UpdateLayer(0);
                btnLay1.setColorFilter(Color.argb(127, 127, 127, 127));
                btnLay2.setColorFilter(Color.argb(127, 127, 127, 127));
                btnLay0.setColorFilter(Color.argb(155, 255, 127, 127));
            }
        });
        btnLay0.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v){
                view.EraseLayer(0);
                return true;
            }
        });

        btnLay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.UpdateLayer(1);
                btnLay0.setColorFilter(Color.argb(127, 127, 127, 127));
                btnLay2.setColorFilter(Color.argb(127, 127, 127, 127));
                btnLay1.setColorFilter(Color.argb(155, 255, 127, 127));

            }
        });
        btnLay1.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v){
                view.EraseLayer(1);
                return true;
            }
        });

        btnLay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.UpdateLayer(2);
                btnLay1.setColorFilter(Color.argb(127, 127, 127, 127));
                btnLay0.setColorFilter(Color.argb(127, 127, 127, 127));
                btnLay2.setColorFilter(Color.argb(155, 255, 127, 127));

            }
        });
        btnLay2.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v){
                view.EraseLayer(2);
                return true;
            }
        });
    }
    private void dispatchTakePictureIntent() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        this.startActivityForResult(cameraIntent, CAM_CODE);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAM_CODE && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap)data.getExtras().get("data");

            v.setBackground(new BitmapDrawable(getResources(), photo));
        }
    }


    public void SetColorButtonText()
    {

    }

}
